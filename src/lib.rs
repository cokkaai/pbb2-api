#![warn(clippy::all)]
#![feature(proc_macro_hygiene, decl_macro)]
#[macro_use] extern crate rocket;

#[macro_use]
extern crate serde_derive;
extern crate serde_json;

#[macro_use]
extern crate log;
extern crate env_logger;

pub mod model;
pub mod service;
pub mod application;
