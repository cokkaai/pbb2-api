// vim:ts=4 expandtab

mod server_configuration;
mod server_configuration_builder;
mod database;
mod calendar_builder;

pub use self::server_configuration::ServerConfiguration;
pub use self::server_configuration_builder::ServerConfigurationBuilder;
pub use self::database::{Database, DatabaseError};
pub use self::database::jfs_database::JfsDatabase;
pub use self::calendar_builder::{CalendarBuilder, CalendarBuilderError};

// use lazy_static;

// lazy_static! {
//     // Define a "singleton" containing server configuration
//     pub static ref CONFIG: ServerConfiguration = {
//             ServerConfiguration::get_instance()
//     };
// }

