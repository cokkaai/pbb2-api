// vim: ts=4 expandtab

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct Blob {
    pub id: String,
    pub data: String,
}
