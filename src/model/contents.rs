use crate::model::{Pharmacy, Document};

#[derive(Serialize, Debug, PartialEq, Clone)]
pub struct Contents<'a> {
    pub my_pharmacy: Option<&'a Pharmacy>,
    pub documents: Vec<&'a Document>,
    pub message: &'a str,
    pub show_time: u32,
    pub fade_time: u32,
    pub color_theme: &'a str,
    pub clock: bool,
    pub version: u32,
}
