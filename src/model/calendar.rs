use crate::model::*;

use chrono::{DateTime, Utc, Duration};

#[derive(Serialize, Debug, PartialEq, Clone)]
pub struct Calendar<'a> {
    pub shifts: Vec<Shift<'a>>,
}

#[derive(Serialize, Debug, PartialEq, Clone, Copy)]
pub struct Period {
    pub start: DateTime<Utc>,
    pub end: DateTime<Utc>,
    #[serde(skip_serializing)]
    len: Duration,
}

#[derive(Serialize, Debug, PartialEq, Clone)]
pub struct Shift<'a> {
    pub period: Period,
    pub pharmacy: &'a Pharmacy,
}

impl<'a> Calendar<'a> {
    pub fn new() -> Calendar<'a> {
        Calendar {
            shifts: Vec::<Shift>::new(),
        }
    }

    pub fn append(&mut self, period: Period, pharmacy: &'a Pharmacy) {
        self.shifts.push(Shift {
            period,
            pharmacy
        });
    }
}

impl Period {
    pub fn new(start: DateTime<Utc>, len: Duration) -> Self {
        Period {
            start,
            end: start + len,
            len
        }
    }

    pub fn in_days(start: DateTime<Utc>, days: u32) -> Self {
        let len = Duration::days(days.into());

        Period {
            start,
            end: start + len,
            len
        }
    }

    pub fn next(&mut self) {
        self.start = self.end;
        self.end = self.end + self.len;
    }

    pub fn prev(&mut self) {
        self.start = self.start - self.len;
        self.end = self.start;
    }
}

impl<'a> Default for Calendar<'a> {
     fn default() -> Self {
        Self::new()
    }
}
