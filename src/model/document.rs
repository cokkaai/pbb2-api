use chrono::prelude::{DateTime, Utc};

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub enum DocumentType {
    CompletePage,
    ImageWithDescription,
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct Document {
    pub id: usize,
    pub description: Option<String>,
    pub publish: Option<DateTime<Utc>>,
    pub revoke: Option<DateTime<Utc>>,
    pub file_name: String,
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct NewDocument {
    pub description: Option<String>,
    pub publish: Option<DateTime<Utc>>,
    pub revoke: Option<DateTime<Utc>>,
    pub file_name: String,
}
