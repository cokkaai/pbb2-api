#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct Log {
    pub message: String,
    context: Vec<ContextData>
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct ContextData {
    pub name: String,
    pub data: String
}
