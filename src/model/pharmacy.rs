#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct Pharmacy {
    pub id: usize,
    pub name: String,
    pub address: String,
    pub sequence: u32,
    pub phone: String,
    pub mine: bool,
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct NewPharmacy {
    pub name: String,
    pub address: String,
    pub sequence: u32,
    pub phone: String,
    pub mine: bool,
}
