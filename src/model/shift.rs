use chrono::offset::Utc;
use chrono::DateTime;

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct ShiftConfiguration {
    pub start: DateTime<Utc>,
    pub days: u32,
}
