use crate::model::*;
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct Configuration {
    pub version: u32,
    pub pharmacies: Vec<Pharmacy>,
    pub documents: Vec<Document>,
    pub shift: Option<ShiftConfiguration>,
    pub presentation: Option<Presentation>,
}

#[allow(dead_code)]
impl Configuration {
    pub fn new() -> Configuration {
        Configuration {
            version: 0,
            pharmacies: Vec::new(),
            documents: Vec::new(),
            shift: Option::None,
            presentation: Option::None,
        }
    }

    pub fn incr_version(&mut self) {
        self.version += 1;
    }
}

impl Default for Configuration {
    fn default() -> Self {
        Self::new()
    }
}