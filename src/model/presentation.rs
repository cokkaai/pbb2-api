// vim: ts=4 expandtab

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct Presentation {
    pub daily_opening_text: String,
    pub on_duty_text: String,
    pub show_time: u32,
    pub fade_time: u32,
    pub color_theme: String,
    pub clock: bool,
    // pub text_color: Color,
    // pub background_color: Color,
}

//#[derive(Serialize, Deserialize, Debug, PartialEq, Clone, Copy)]
// pub struct Color {
//    pub red: u8,
//    pub green: u8,
//    pub blue: u8,
// }
