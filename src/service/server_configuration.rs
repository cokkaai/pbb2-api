use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ServerConfiguration {
    pub document_root: String,
    pub image_path: String,
    pub thumb_path: String,
    pub thumb_size: u32,
    pub database_path: String,
}

#[allow(dead_code)]
impl ServerConfiguration {
    pub fn new() -> ServerConfiguration {
        ServerConfiguration {
            document_root: String::new(),
            image_path: String::new(),
            thumb_path: String::new(),
            thumb_size: 0,
            database_path: String::new(),
        }
    }

    pub fn parse(data: &str) -> Result<ServerConfiguration, serde_json::Error> {
        serde_json::from_str(data)
    }
}

    impl Default for ServerConfiguration {
    fn default() -> Self {
        Self::new()
    }
}

#[cfg(test)]
mod tests {
    use super::ServerConfiguration;

    #[test]
    fn should_deserialize() {
        let data = "{\"document_root\":\"/var/www/pbb/app\",\"image_path\":\"/var/www/pbb/images\",\"thumb_path\":\"/var/www/pbb/thumbs\",\"thumb_size\":100,\"database_path\":\"data\"}";

        let conf = ServerConfiguration::parse(data).unwrap();

        assert_eq!(conf.document_root, "/var/www/pbb/app");
        assert_eq!(conf.image_path, "/var/www/pbb/images");
        assert_eq!(conf.thumb_path, "/var/www/pbb/thumbs");
        assert_eq!(conf.thumb_size, 100);
        assert_eq!(conf.database_path, "data");
    }

    #[test]
    fn invalid_files_return_no_configuration() {
        let empty = ServerConfiguration::parse("");
        assert_eq!(empty.is_err(), true);

        let invalid = ServerConfiguration::parse("this is not json data");
        assert_eq!(invalid.is_err(), true);
    }
}
