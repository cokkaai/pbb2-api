mod configuration_type;
mod storage;

use self::storage::Storage;
use crate::model::*;
use crate::service::{Database, DatabaseError};

use std::sync;

use self::configuration_type::*;

/// The database implementation
pub struct JfsDatabase {
    storage: Storage,
    pub draft: sync::RwLock<Configuration>,
    pub release: sync::RwLock<Configuration>,
}

#[allow(dead_code, unused)]
impl JfsDatabase {
    const DEFAULT_PATH: &'static str = "data";

    pub fn new() -> JfsDatabase {
        JfsDatabase::new_with_path(JfsDatabase::DEFAULT_PATH)
    }

    pub fn new_with_path(path: &str) -> JfsDatabase {
        // Instantiate storage service
        let storage = Storage::new_with_path(path).unwrap();

        // Load or create the draft configuration
        let draft = match storage.get(Config::Draft.get_name()) {
            Ok(data) => data,
            Err(_) => Configuration::new(),
        };

        // Load or create the release configuration
        let release = match storage.get(Config::Release.get_name()) {
            Ok(data) => data,
            Err(_) => Configuration::new(),
        };

        JfsDatabase {
            storage,
            draft: sync::RwLock::new(draft),
            release: sync::RwLock::new(release),
        }
    }

    fn read_configuration(
        &self,
        config: Config,
    ) -> Result<sync::RwLockReadGuard<Configuration>, DatabaseError> {
        let lock = match config {
            Config::Draft => self.draft.read(),
            Config::Release => self.release.read(),
        };

        match lock {
            Ok(guard) => Ok(guard),
            Err(_) => {
                let msg = format!(
                    "Cannot lock {} configuration for reading because the lock is poisoned.",
                    config.get_name()
                );
                error!("{}", &msg);
                Err(DatabaseError::Error { message: msg })
            }
        }
    }

    fn read_draft(&self) -> Result<sync::RwLockReadGuard<Configuration>, DatabaseError> {
        self.read_configuration(Config::Draft)
    }

    fn read_release(&self) -> Result<sync::RwLockReadGuard<Configuration>, DatabaseError> {
        self.read_configuration(Config::Release)
    }

    fn write_configuration(
        &self,
        config: Config,
    ) -> Result<sync::RwLockWriteGuard<Configuration>, DatabaseError> {
        let lock = match config {
            Config::Draft => self.draft.write(),
            Config::Release => self.release.write(),
        };

        match lock {
            Ok(guard) => Ok(guard),
            Err(_) => {
                let msg = format!(
                    "Cannot lock {} configuration for writing because the lock is poisoned.",
                    config.get_name()
                );
                error!("{}", &msg);
                Err(DatabaseError::Error { message: msg })
            }
        }
    }

    fn write_draft(&self) -> Result<sync::RwLockWriteGuard<Configuration>, DatabaseError> {
        self.write_configuration(Config::Draft)
    }

    fn write_release(&self) -> Result<sync::RwLockWriteGuard<Configuration>, DatabaseError> {
        self.write_configuration(Config::Release)
    }

    /// Find a pharmacy by id
    fn find_pharmacy(conf: &Configuration, id: usize) -> Option<usize> {
        for i in 0..conf.pharmacies.len() {
            if conf.pharmacies[i].id == id {
                info!("Found pharmacy with id {} at position {}", id, i);
                return Some(i);
            }
        }

        info!("Pharmacy with id {} not found", id);
        None
    }

    /// Find a document by id
    fn find_document(conf: &Configuration, id: usize) -> Option<usize> {
        for i in 0..conf.documents.len() {
            if conf.documents[i].id == id {
                return Some(i);
            }
        }

        None
    }
}

impl Default for JfsDatabase {
    fn default() -> Self {
        Self::new()
    }
}

impl Database for JfsDatabase {
    fn get_draft_configuration(&self) -> Result<Configuration, DatabaseError> {
        match self.read_draft() {
            Ok(guard) => {
                let conf = (*guard).clone();
                debug!("Read draft configuration data {:?}", conf);
                Ok(conf)
            }
            Err(e) => Err(e),
        }
    }

    fn get_release_configuration(&self) -> Result<Configuration, DatabaseError> {
        match self.read_release() {
            Ok(guard) => Ok((*guard).clone()),
            Err(e) => Err(e),
        }
    }

    fn store_draft_configuration(&self, config: Configuration) -> Result<(), DatabaseError> {
        // Save configuration
        let id = self.storage.save(Config::Draft.get_name(), &config)?;

        // Reload configuration data
        let config = self.storage.get(id.as_str())?;

        // Update cache
        let mut draft = self.write_draft()?;
        *draft = config;
        Ok(())
    }

    fn store_release_configuration(&self, config: Configuration) -> Result<(), DatabaseError> {
        // Save configuration
        let id = self.storage.save(Config::Release.get_name(), &config)?;

        // Reload configuration data
        let config = self.storage.get(id.as_str())?;

        // Update cache
        let mut release = self.write_release()?;
        *release = config;
        Ok(())
    }

    // ==========  Documents related fns  ==========

    /// Return the list of registered documents
    fn get_documents(&self) -> Result<Vec<Document>, DatabaseError> {
        let conf = self.read_draft()?;
        Ok(conf.documents.clone())
    }

    fn get_document(&self, id: usize) -> Result<Option<Document>, DatabaseError> {
        let conf = self.read_draft()?;

        match Self::find_document(&conf, id) {
            Some(i) => Ok(Some(conf.documents[i].clone())),
            None => Err(DatabaseError::InvalidId),
        }
    }

    /// Creates a new document.
    /// Returns the registered document.
    fn create_document(&self, document: &NewDocument) -> Result<usize, DatabaseError> {
        let mut conf = self.write_draft()?;

        let id: usize = match conf.documents.iter().map(|x| x.id).max() {
            Some(i) => i + 1,
            None => 1,
        };

        let document = Document {
            id,
            description: document.description.clone(),
            publish: document.publish,
            revoke: document.revoke,
            file_name: document.file_name.clone(),
        };

        conf.documents.push(document);
        conf.incr_version();
        self.storage.save(Config::Draft.get_name(), &conf)?;
        Ok(id)
    }

    fn update_document(&self, document: &Document) -> Result<(), DatabaseError> {
        let mut conf = self.write_draft()?;

        // Update the draft configuration then save
        match Self::find_document(&conf, document.id) {
            Some(i) => {
                conf.documents[i] = document.clone();
                conf.incr_version(); 
                self.storage.save(Config::Draft.get_name(), &conf)?;
                Ok(())
            }
            None => Err(DatabaseError::InvalidId),
        }
    }

    fn delete_document(&self, id: usize) -> Result<Document, DatabaseError> {
        let mut conf = self.write_draft()?;

        match Self::find_document(&conf, id) {
            Some(i) => {
                let deleted = conf.documents.remove(i);
                conf.incr_version();
                self.storage.save(Config::Draft.get_name(), &conf)?;
                Ok(deleted)
            }
            None => Err(DatabaseError::InvalidId),
        }
    }

    // ==========  Pharmacies related fns  ==========

    fn get_pharmacies(&self) -> Result<Vec<Pharmacy>, DatabaseError> {
        let conf = self.read_draft()?;
        Ok(conf.pharmacies.clone())
    }

    fn get_pharmacy(&self, id: usize) -> Result<Option<Pharmacy>, DatabaseError> {
        let conf = self.read_draft()?;
        match Self::find_pharmacy(&conf, id) {
            Some(i) => Ok(Some(conf.pharmacies[i].clone())),
            //None => Err(DatabaseError::InvalidId),
            None => Ok(None),
        }
    }

    fn create_pharmacy(&self, pharmacy: &NewPharmacy) -> Result<usize, DatabaseError> {
        let mut conf = self.write_draft()?;

        let id: usize = match conf.pharmacies.iter().map(|x| x.id).max() {
            Some(i) => i + 1,
            None => 1,
        };

        let pharmacy = Pharmacy {
            id,
            name: pharmacy.name.clone(),
            address: pharmacy.address.clone(),
            sequence: pharmacy.sequence,
            phone: pharmacy.phone.clone(),
            mine: pharmacy.mine,
        };

        if pharmacy.mine {
            for pharmacy in conf.pharmacies.iter_mut() {
                pharmacy.mine = false;
            }
        }

        conf.pharmacies.push(pharmacy);
        conf.incr_version();
        self.storage.save(Config::Draft.get_name(), &conf)?;
        Ok(id)
    }

    fn update_pharmacy(&self, pharmacy: &Pharmacy) -> Result<(), DatabaseError> {
        let mut conf = self.write_draft()?;

        // Update the draft configuration then save
        match Self::find_pharmacy(&conf, pharmacy.id) {
            Some(i) => {
                if pharmacy.mine {
                    for pharmacy in conf.pharmacies.iter_mut() {
                        pharmacy.mine = false;
                    }
                }

                conf.pharmacies[i] = pharmacy.clone();
                conf.incr_version();
                self.storage.save(Config::Draft.get_name(), &conf)?;
                Ok(())
            }
            None => Err(DatabaseError::InvalidId),
        }
    }

    fn delete_pharmacy(&self, id: usize) -> Result<Pharmacy, DatabaseError> {
        let mut conf = self.write_draft()?;

        match Self::find_pharmacy(&conf, id) {
            Some(i) => {
                let deleted = conf.pharmacies.remove(i);
                conf.incr_version();
                self.storage.save(Config::Draft.get_name(), &conf)?;
                Ok(deleted)
            }
            None => Err(DatabaseError::InvalidId),
        }
    }

    fn get_shift(&self) -> Result<Option<ShiftConfiguration>, DatabaseError> {
        let conf = self.read_draft()?;
        Ok(conf.shift.clone())
    }

    fn store_shift(&self, shift: &ShiftConfiguration) -> Result<(), DatabaseError> {
        let mut conf = self.write_draft()?;
        conf.shift = Some(shift.clone());
        conf.incr_version();
        self.storage.save(Config::Draft.get_name(), &conf)?;
        Ok(())
    }

    fn get_presentation(&self) -> Result<Option<Presentation>, DatabaseError> {
        let conf = self.read_draft()?;
        Ok(conf.presentation.clone())
    }

    fn store_presentation(&self, presentation: &Presentation) -> Result<(), DatabaseError> {
        let mut conf = self.write_draft()?;
        conf.presentation = Some(presentation.clone());
        conf.incr_version();
        self.storage.save(Config::Draft.get_name(), &conf)?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::model::*;
    use crate::service::{Database, DatabaseError, JfsDatabase};
    use chrono::prelude::*;
    use std::fs;

    fn clear_directory(data_dir: &str) {
        fs::DirBuilder::new()
            .recursive(true)
            .create(&data_dir)
            .unwrap();

        for entry in fs::read_dir(&data_dir).unwrap() {
            let entry = entry.unwrap();
            fs::remove_file(entry.path()).unwrap();
        }
    }

    fn test_path(test_code: &str) -> String {
        String::from("./data/test/") + test_code
    }

    fn setup_test_database(code: &str) -> JfsDatabase {
        let path = test_path(code);
        clear_directory(&path);
        JfsDatabase::new_with_path(&path)
    }

    fn populate_test_database(code: &str, size: usize) -> JfsDatabase {
        let database = setup_test_database(code);
        let draft = create_random_config(size);
        database.store_draft_configuration(draft).unwrap();
        database
    }

    fn open_test_database(code: &str) -> JfsDatabase {
        let path = test_path(code);
        JfsDatabase::new_with_path(&path)
    }

    fn assert_document(reference: &Document, checked: &Document) {
        assert_eq!(reference.description, checked.description);
        assert_eq!(reference.file_name, checked.file_name);
        assert_eq!(reference.id, checked.id);
        assert_eq!(reference.publish, checked.publish);
        assert_eq!(reference.revoke, checked.revoke);
    }

    fn assert_pharmacy(reference: &Pharmacy, checked: &Pharmacy) {
        assert_eq!(reference.address, checked.address);
        assert_eq!(reference.id, checked.id);
        assert_eq!(reference.mine, checked.mine);
        assert_eq!(reference.name, checked.name);
        assert_eq!(reference.phone, checked.phone);
        assert_eq!(reference.sequence, checked.sequence);
    }

    fn create_random_config(size: usize) -> Configuration {
        let mut config = Configuration::new();

        for id in 0..size {
            config.documents.push(Document {
                id: id,
                description: Some(format!("test document {}", id)),
                publish: Some(Utc.ymd(2017, 2, 1).and_hms(8, 0, 0)),
                revoke: Some(Utc.ymd(2017, 3, 1).and_hms(8, 0, 0)),
                file_name: format!("test_document_{}.jpg", id),
            });

            config.pharmacies.push(Pharmacy {
                id: id,
                name: format!("test pharmacy {}", id),
                address: "street address whatever".to_owned(),
                sequence: id as u32,
                phone: "phone number".to_owned(),
                mine: false,
            });
        }

        config
    }

    #[test]
    pub fn database_001_new_with_path() {
        let _database = setup_test_database("database_001_new_with_path");
    }

    #[test]
    pub fn database_002_configuration() {
        let code = "database_002_configuration";
        let database = setup_test_database(code);

        // Add some data to the draft configuration
        let draft = Configuration::new();

        // Add some data to the release configuration
        let release = Configuration::new();

        // Store the configuration
        database.store_draft_configuration(draft).unwrap();
        database.store_release_configuration(release).unwrap();

        // Reload data from disk
        let database2 = JfsDatabase::new_with_path(&test_path(code));

        // Check some data
        let _draft2 = database2.get_draft_configuration();

        let _release2 = database2.get_release_configuration();
    }

    #[test]
    pub fn database_004_get_documents() {
        let database = populate_test_database("database_004_get_documents", 5);
        let documents = database.get_documents();

        assert_eq!(documents.unwrap().len(), 5);
    }

    #[test]
    pub fn database_005_get_document() {
        let database = populate_test_database("database_005_get_document", 5);
        let document = database.get_document(1).unwrap().unwrap();

        assert_eq!(document.id, 1);
        assert_eq!(document.description, Some("test document 1".to_owned()));
        assert_eq!(document.file_name, "test_document_1.jpg");
        assert_eq!(document.publish, Some(Utc.ymd(2017, 2, 1).and_hms(8, 0, 0)));
        assert_eq!(document.revoke, Some(Utc.ymd(2017, 3, 1).and_hms(8, 0, 0)));
    }

    #[test]
    pub fn database_006_create_document() {
        let code = "database_006_create_document";

        // Create document
        let database = populate_test_database(code, 5);
        let document = NewDocument {
            description: Some(code.to_owned()),
            publish: Some(Utc.ymd(2017, 2, 1).and_hms(8, 0, 0)),
            revoke: Some(Utc.ymd(2017, 3, 1).and_hms(8, 0, 0)),
            file_name: format!("{}.jpg", code),
        };
        let id = database.create_document(&document).unwrap();

        // Fetch created document
        let database2 = open_test_database(code);
        let document2 = database2.get_document(id).unwrap().unwrap();

        assert_eq!(&document.description, &document2.description);
        assert_eq!(&document.file_name, &document2.file_name);
        assert_eq!(&document.publish, &document2.publish);
        assert_eq!(&document.revoke, &document2.revoke);
    }

    #[test]
    pub fn database_007_update_document() {
        let code = "database_007_update_document";

        // Get document #1
        let database = populate_test_database(code, 5);
        let mut document = database.get_document(1).unwrap().unwrap();

        // Update & save document #1
        document.description = Some("Updated".to_owned());
        document.file_name = "updated.pdf".to_owned();
        let _result = database.update_document(&document);

        // Fetch created document
        let database2 = open_test_database(code);
        let document2 = database2.get_document(document.id).unwrap().unwrap();

        assert_document(&document, &document2);
    }

    #[test]
    pub fn database_008_delete_document() {
        let code = "database_008_delete_document";
        let id = 1;

        // Delete document
        let database = populate_test_database(code, 5);
        database.delete_document(id).unwrap();

        // Try to find the delete document
        let database2 = open_test_database(code);
        match database2.get_document(id) {
            Ok(Some(_)) => panic!("Document should be deleted"),
            Ok(None) => (),
            Err(DatabaseError::InvalidId) => (),
            Err(_) => panic!("Document deletion failed"),
        };
    }

    #[test]
    pub fn database_009_get_pharmacies() {
        let database = populate_test_database("database_009_get_pharmacies", 5);
        let pharmacies = database.get_pharmacies().unwrap();

        assert_eq!(pharmacies.len(), 5);
    }

    #[test]
    pub fn database_010_get_pharmacy() {
        let database = populate_test_database("database_010_get_pharmacy", 5);
        let pharmacy = database.get_pharmacy(1).unwrap().unwrap();

        assert_eq!(pharmacy.id, 1);
        assert_eq!(pharmacy.address, "street address whatever");
        assert_eq!(pharmacy.mine, false);
        assert_eq!(pharmacy.name, "test pharmacy 1");
        assert_eq!(pharmacy.phone, "phone number");
        assert_eq!(pharmacy.sequence, 1);
    }

    #[test]
    pub fn database_011_create_pharmacy() {
        let code = "database_011_create_pharmacy";

        // Create pharmacy
        let database = populate_test_database(code, 5);
        let pharmacy = NewPharmacy {
            name: "New pharmacy".to_owned(),
            address: "New pharmacy street address".to_owned(),
            sequence: 11,
            phone: "New pharmacy phone number".to_owned(),
            mine: false,
        };
        let id = database.create_pharmacy(&pharmacy).unwrap();

        // Fetch created pharmacy
        let database2 = open_test_database(code);
        let pharmacy2 = database2.get_pharmacy(id).unwrap().unwrap();

        assert_eq!(&pharmacy.address, &pharmacy2.address);
        assert_eq!(&pharmacy.mine, &pharmacy2.mine);
        assert_eq!(&pharmacy.name, &pharmacy2.name);
        assert_eq!(&pharmacy.phone, &pharmacy2.phone);
        assert_eq!(&pharmacy.sequence, &pharmacy2.sequence);
    }

    #[test]
    pub fn database_012_update_pharmacy() {
        let code = "database_012_update_pharmacy";

        // Get pharmacy #1
        let database = populate_test_database(code, 5);
        let mut pharmacy = database.get_pharmacy(1).unwrap().unwrap();

        // Update & save pharmacy #1
        pharmacy.address = "Updated address".to_owned();
        pharmacy.name = "Updated name".to_owned();
        pharmacy.mine = true;
        pharmacy.phone = "Updated phone".to_owned();
        pharmacy.sequence = 12;
        database.update_pharmacy(&pharmacy).unwrap();

        // Fetch created pharmacy
        let database2 = open_test_database(code);
        let pharmacy2 = database2.get_pharmacy(pharmacy.id).unwrap().unwrap();

        assert_pharmacy(&pharmacy, &pharmacy2);
    }

    #[test]
    pub fn database_013_delete_pharmacy() {
        let code = "database_013_delete_pharmacy";
        let id = 1;

        // Delete pharmacy
        let database = populate_test_database(code, 5);
        database.delete_pharmacy(id).unwrap();

        // Try to find the delete document
        let _database2 = open_test_database(code);
        match database.get_pharmacy(id) {
            Ok(Some(_)) => panic!("Pharmacy should be deleted"),
            Ok(None) => (),
            Err(_) => panic!("Pharmacy lookup failed"),
        };
    }
}
