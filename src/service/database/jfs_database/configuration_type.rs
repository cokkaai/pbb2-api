pub enum Config {
    Draft,
    Release
}

pub trait NameProvider {
    fn get_name(&self) -> &'static str;
}

impl NameProvider for Config {
    fn get_name(&self) -> &'static str {
        match self {
            Config::Draft => "draft",
            Config::Release => "release",
        }
    }
}
