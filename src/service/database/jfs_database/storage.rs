// vim:ts=4 expandtab

use crate::model::*;

use std::fs;
use std::io;
use std::path::Path;
use std::vec::Vec;

use regex::Regex;

use chrono::prelude::*;

pub struct Storage {
    database_dir: String,
    database_path: String,
    database: jfs::Store,
}

#[allow(dead_code)]
impl Storage {
    /// Open the database in the specified directory.
    /// If the database does not exists, a new one is created.
    pub fn new_with_path(database_dir: &str) -> Result<Storage, io::Error> {
        // Require well formatted json database file
        let config = jfs::Config {
            indent: 2,
            pretty: true,
            single: true,
        };

        // Create data directory. Exit on failure
        if let Err(e) = fs::DirBuilder::new().recursive(true).create(&database_dir) {
            error!("Cannot create database directory: {:?}", e);
            return Err(e);
        }

        // Try to open database
        let path = format!("{}/database.json", database_dir);
        let mut database = Self::open_database(&path, config);

        // Try to open a backup
        if database.is_none() {
            if let Ok(backups) = Self::find_backups(database_dir) {
                for path in backups {
                    database = Self::open_database(&path, config);
                    if database.is_some() {
                        break;
                    }
                }
            }
        }

        // Found no database, create an empty one.
        if database.is_none() {
            database = match jfs::Store::new_with_cfg(&path, config) {
                Ok(database) => {
                    info!("Created empty database {}", path);
                    Some(database)
                }
                Err(e) => {
                    error!("Cannot create database: {:?}", e);
                    return Err(e);
                }
            };
        }

        Ok(Storage {
            database_dir: database_dir.to_owned(),
            database_path: format!("{}/database.json", database_dir),
            database: database.unwrap(),
        })
    }

    fn open_database(path: &str, config: jfs::Config) -> Option<jfs::Store> {
        if Path::new(&path).exists() {
            let store = jfs::Store::new_with_cfg(&path, config);

            match store {
                Ok(database) => {
                    info!("Using database: {}", path);
                    Some(database)
                }
                Err(e) => {
                    error!("Error opening database: {:?}", e);
                    None
                }
            }
        } else {
            None
        }
    }

    /// Find all files with pattern database-yyyymmdd-hhmmss-ns.json
    /// and sort them from the newest to the oldest.
    fn find_backups(dir: &str) -> Result<Vec<String>, io::Error> {
        let re = Regex::new(r"^database-\d{8}-\d{6}-\d+\.json$").unwrap();
        let mut files = Vec::new();

        for entry in fs::read_dir(Path::new(dir))? {
            let path = entry?.path();

            if let Some(file_name) = path.file_name() {
                if let Some(file_name) = file_name.to_str() {
                    if re.is_match(file_name) {
                        files.push(format!("{}", path.display()));
                    }
                }
            }
        }

        files.sort_by(|a, b| a.cmp(b).reverse());
        Ok(files)
    }

    /// Gets the value corresponding to the key.
    pub fn get(&self, key: &str) -> Result<Configuration, io::Error> {
        self.database.get(key)
    }

    /// Returns the timestamp of latest database change.
    fn get_database_timestamp(&self) -> DateTime<Utc> {
        match fs::metadata(&self.database_path) {
            Ok(metadata) => DateTime::from(metadata.modified().unwrap()),
            Err(_) => Utc::now(),
        }
    }

    /// Save the database creating a backup of the previous one.
    pub fn save(&self, key: &str, config: &Configuration) -> io::Result<String> {
        // Versioning database files on second fraction depending
        // on filesystem timestamp resolution it is *not* safe
        // and may fail on some platforms.
        // In particular it can affects storage_003 test.

        let new_path = format!(
            "{}/database-{}.json",
            self.database_dir,
            self.get_database_timestamp().format("%Y%m%d-%H%M%S-%f")
        );

        fs::copy(&self.database_path, &new_path)?;

        let result = self.database.save_with_id(config, key);

        if result.is_err() {
            error!("Cannot save database {}: {:?}", &new_path, result.as_ref());
        }

        result
    }
}

#[cfg(test)]
mod tests {
    use super::Storage;
    use crate::model::*;
    use crate::service::database::jfs_database::*;
    use regex::Regex;
    use std::fs;

    fn empty_directory(test_code: &str) -> String {
        let dir = "../data/test/".to_owned() + test_code;

        fs::DirBuilder::new().recursive(true).create(&dir).unwrap();

        for entry in fs::read_dir(&dir).unwrap() {
            let entry = entry.unwrap();
            fs::remove_file(entry.path()).unwrap();
        }

        dir
    }

    #[test]
    pub fn storage_001_creating_a_database_must_create_an_empty_store() {
        let test_dir = empty_directory("storage_001");
        let _ = Storage::new_with_path(&test_dir).unwrap();

        let mut count = 0;
        let mut found = false;

        for entry in fs::read_dir(&test_dir).unwrap() {
            count += 1;

            let path = entry.unwrap().path();
            let path = path.to_str().unwrap();

            if format!("{}/database.json", &test_dir) == path {
                found = true;
            }
        }

        assert!(1 == count, "Found more than one file.");
        assert!(found, "Missing database.json file.");
    }

    #[test]
    pub fn storage_002_saving_the_database_must_backup_the_previous_content() {
        let test_dir = empty_directory("storage_002");
        let storage = Storage::new_with_path(&test_dir).unwrap();
        let backup_re = Regex::new(r"^database-\d{8}-\d{6}-\d+\.json$").unwrap();

        let _ = storage.save(Config::Draft.get_name(), &Configuration::new());

        let mut count = 0;
        let mut database_found = false;
        let mut backup_found = 0;

        for entry in fs::read_dir(&test_dir).unwrap() {
            count += 1;

            let path = entry.unwrap().path();
            let file_name = path.file_name().unwrap().to_str().unwrap();

            if file_name == "database.json" {
                database_found = true;
            } else if backup_re.is_match(file_name) {
                backup_found += 1;
            }
        }

        assert!(2 == count, "Expecting two files.");
        assert!(database_found, "Missing database.json.");
        assert!(backup_found == 1, "Expecting one backup.");
    }

    // #[test]
    // This test is disabled. The save method must be improved.
    #[allow(dead_code)]
    pub fn storage_003_any_time_you_save_a_new_backup_is_created() {
        let test_dir = empty_directory("storage_003");
        let storage = Storage::new_with_path(&test_dir).unwrap();
        let backup_re = Regex::new(r"^database-\d{8}-\d{6}-\d+\.json$").unwrap();

        let mut conf = Configuration::new();
        storage.save("conf1", &conf).unwrap();

        conf.presentation = Some(Presentation {
            clock: false,
            color_theme: "".to_owned(),
            daily_opening_text: "".to_owned(),
            fade_time: 4,
            on_duty_text: "".to_owned(),
            show_time: 20,
        });
        storage.save("conf1", &conf).unwrap();

        let mut count = 0;
        let mut database_found = false;
        let mut backup_found = 0;

        for entry in fs::read_dir(&test_dir).unwrap() {
            count += 1;

            let path = entry.unwrap().path();
            let file_name = path.file_name().unwrap().to_str().unwrap();

            if file_name == "database.json" {
                database_found = true;
            } else if backup_re.is_match(file_name) {
                backup_found += 1;
            }
        }

        assert!(3 == count, "Expecting three files.");
        assert!(database_found, "Missing database.json.");
        assert!(backup_found == 2, "Expecting two backups.");
    }

    #[test]
    pub fn storage_004_should_use_the_backup_when_the_database_cannot_be_opened() {
        let test_dir = empty_directory("storage_004");
        let storage = Storage::new_with_path(&test_dir).unwrap();

        // Creates a database and a backup file.

        let mut presentation = Presentation {
            clock: false,
            color_theme: "".to_owned(),
            daily_opening_text: "".to_owned(),
            fade_time: 4,
            on_duty_text: "".to_owned(),
            show_time: 10,
        };

        {
            let mut conf = Configuration::new();
            conf.presentation = Some(presentation.clone());
            storage.save("conf1", &conf).unwrap();

            let conf = storage.get("conf1").unwrap();
            assert!(
                conf.presentation.unwrap().show_time == 10,
                "Configuration mismatch"
            );
        }

        {
            let mut conf = Configuration::new();
            presentation.show_time = 20;
            conf.presentation = Some(presentation);
            storage.save("conf1", &conf).unwrap();

            let conf = storage.get("conf1").unwrap();
            assert!(
                conf.presentation.unwrap().show_time == 20,
                "Configuration mismatch"
            );
        }

        fs::remove_file(format!("{}/database.json", test_dir)).unwrap();
        let storage = Storage::new_with_path(&test_dir).unwrap();
        let conf = storage.get("conf1").unwrap();

        assert!(
            conf.presentation.unwrap().show_time == 10,
            "Expecting to pick up the older configuration"
        );
    }
}
