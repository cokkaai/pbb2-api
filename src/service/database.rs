pub mod jfs_database;

use crate::model::*;
use std::convert;
use std::fmt;
use std::io;

pub trait Database {
    fn get_draft_configuration(&self) -> Result<Configuration, DatabaseError>;
    fn store_draft_configuration(&self, config: Configuration) -> Result<(), DatabaseError>;

    fn get_release_configuration(&self) -> Result<Configuration, DatabaseError>;
    fn store_release_configuration(&self, config: Configuration) -> Result<(), DatabaseError>;

    fn get_documents(&self) -> Result<Vec<Document>, DatabaseError>;
    fn get_document(&self, id: usize) -> Result<Option<Document>, DatabaseError>;
    fn create_document(&self, document: &NewDocument) -> Result<usize, DatabaseError>;
    fn update_document(&self, document: &Document) -> Result<(), DatabaseError>;
    fn delete_document(&self, id: usize) -> Result<Document, DatabaseError>;

    fn get_pharmacies(&self) -> Result<Vec<Pharmacy>, DatabaseError>;
    fn get_pharmacy(&self, id: usize) -> Result<Option<Pharmacy>, DatabaseError>;
    fn create_pharmacy(&self, pharmacy: &NewPharmacy) -> Result<usize, DatabaseError>;
    fn update_pharmacy(&self, pharmacy: &Pharmacy) -> Result<(), DatabaseError>;
    fn delete_pharmacy(&self, id: usize) -> Result<Pharmacy, DatabaseError>;

    fn get_shift(&self) -> Result<Option<ShiftConfiguration>, DatabaseError>;
    fn store_shift(&self, shift: &ShiftConfiguration) -> Result<(), DatabaseError>;

    fn get_presentation(&self) -> Result<Option<Presentation>, DatabaseError>;
    fn store_presentation(&self, shift: &Presentation) -> Result<(), DatabaseError>;
}

#[derive(Debug)]
pub enum DatabaseError {
    IoError { base_error: std::io::Error },
    Error { message: String },
    BadRequest { reason: &'static str },
    InvalidId,
    DuplicatedEntry,
}

impl convert::From<io::Error> for DatabaseError {
    fn from(error: io::Error) -> Self {
        DatabaseError::IoError { base_error: error }
    }
}

impl fmt::Display for DatabaseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            DatabaseError::IoError { base_error } => write!(f, "{}", base_error),
            DatabaseError::Error { message } => write!(f, "{}", message),
            DatabaseError::BadRequest { reason } => write!(f, "Bad request: {}", reason),
            DatabaseError::InvalidId => write!(f, "Invalid id"),
            DatabaseError::DuplicatedEntry => write!(f, "Duplicated entry"),
        }
    }
}
