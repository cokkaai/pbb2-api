use super::server_configuration::ServerConfiguration;
use std::io::prelude::*;

/// Builder for server configuration
#[derive(Debug, Serialize, Deserialize)]
pub struct ServerConfigurationBuilder {
    document_root: String,
    image_path: String,
    thumb_path: String,
    thumb_size: u32,
    database_path: String,
}

#[allow(dead_code)]
impl ServerConfigurationBuilder {
    const DEFAULT_DOCUMENT_ROOT: &'static str = "/var/www/pbb/app";
    const DEFAULT_IMAGE_PATH: &'static str = "/var/www/pbb/images";
    const DEFAULT_THUMB_PATH: &'static str = "/var/www/pbb/thumbs";
    const DEFAULT_THUMB_SIZE: u32 = 200;
    const DEFAULT_DATABASE_PATH: &'static str = "/var/lib/pbb/data";

    const PBB_DOCUMENT_ROOT: &'static str = "PBB_DOCUMENT_ROOT";
    const PBB_IMAGE_PATH: &'static str = "PBB_IMAGE_PATH";
    const PBB_THUMB_PATH: &'static str = "PBB_THUMB_PATH";
    const PBB_THUMB_SIZE: &'static str = "PBB_THUMB_SIZE";
    const PBB_DATABASE_PATH: &'static str = "PBB_DATABASE_PATH";
    
    pub fn new() -> ServerConfigurationBuilder {
        ServerConfigurationBuilder {
            document_root: Self::DEFAULT_DOCUMENT_ROOT.to_owned(),
            image_path: Self::DEFAULT_IMAGE_PATH.to_owned(),
            thumb_path: Self::DEFAULT_THUMB_PATH.to_owned(),
            thumb_size: Self::DEFAULT_THUMB_SIZE.to_owned(),
            database_path: Self::DEFAULT_DATABASE_PATH.to_owned(),
        }
    }

    /// Set the document root.
    pub fn with_document_root(mut self, document_root: &str) -> ServerConfigurationBuilder {
        self.document_root = document_root.to_owned();
        self
    }

    /// Set the directory for user-uploaded images.
    pub fn with_image_path(mut self, image_path: &str) -> ServerConfigurationBuilder {
        self.image_path = image_path.to_owned();
        self
    }

    /// Set the directory for user-uploaded image thumbs.
    pub fn with_thumb_path(mut self, thumb_path: &str) -> ServerConfigurationBuilder {
        self.thumb_path = thumb_path.to_owned();
        self
    }

    /// Set the thumbnail size.
    pub fn with_thumb_size(mut self, thumb_size: u32) -> ServerConfigurationBuilder {
        self.thumb_size = thumb_size;
        self
    }

    /// Set the database location (directory).
    pub fn with_database_path(mut self, database_path: &str) -> ServerConfigurationBuilder {
        self.database_path = database_path.to_owned();
        self
    }

    /// Read configuration from a JSON file.
    pub fn read_file(mut self, file: &str) -> ServerConfigurationBuilder {
        let mut buffer = String::new();

        if let Ok(mut fd) = std::fs::File::open(file) {
            if fd.read_to_string(&mut buffer).is_ok() {
                if let Ok(data) = serde_json::from_str::<ServerConfigurationBuilder>(&buffer) {
                    info!("Using configuration file {}.", file);
                    self.document_root = data.document_root;
                    self.image_path = data.image_path;
                    self.thumb_path = data.thumb_path;
                    self.thumb_size = data.thumb_size;
                    self.database_path = data.database_path;
                } else {
                    buffer.clear();
                }
            }
        }

        self
    }

    /// Read configuration from environment variables.
    pub fn from_env() -> ServerConfigurationBuilder {
        let mut builder = ServerConfigurationBuilder::new();

        if let Ok(value) = std::env::var(Self::PBB_DOCUMENT_ROOT) {
            builder.document_root = value;
        }

        if let Ok(value) = std::env::var(Self::PBB_IMAGE_PATH) {
            builder.image_path = value;
        }

        if let Ok(value) = std::env::var(Self::PBB_THUMB_PATH) {
            builder.thumb_path = value;
        }

        if let Ok(value) = std::env::var(Self::PBB_THUMB_SIZE) {
            builder.thumb_size = match value.parse::<u32>() {
                Ok(size) => size,
                Err(_) => Self::DEFAULT_THUMB_SIZE,
            };
        }

        if let Ok(value) = std::env::var(Self::PBB_DATABASE_PATH) {
            builder.database_path = value;
        }

        builder
    }

    /// Create the server configuration.
    pub fn build(self) -> ServerConfiguration {
        ServerConfiguration {
            document_root: self.document_root,
            image_path: self.image_path,
            thumb_path: self.thumb_path,
            thumb_size: self.thumb_size,
            database_path: self.database_path,
        }
    }
}

impl Default for ServerConfigurationBuilder {
    fn default() -> Self {
        Self::new()
    }
}
    
#[cfg(test)]
mod tests {
    use super::ServerConfigurationBuilder;
    use std::io::prelude::*;

    const DOCUMENT_ROOT: &str = "/test/app";
    const IMAGES_DIR: &str = "/test/images";
    const THUMBS_DIR: &str = "/test/thumbs";
    const THUMB_SIZE: u32 = 150;
    const DATA_DIR: &str = "/test/data";

    #[test]
    fn manually_create_a_configuration() {
        let conf = ServerConfigurationBuilder::new()
            .with_document_root(DOCUMENT_ROOT)
            .with_image_path(IMAGES_DIR)
            .with_thumb_path(THUMBS_DIR)
            .with_thumb_size(THUMB_SIZE)
            .with_database_path(DATA_DIR)
            .build();

        assert_eq!(conf.document_root, DOCUMENT_ROOT);
        assert_eq!(conf.image_path, IMAGES_DIR);
        assert_eq!(conf.thumb_path, THUMBS_DIR);
        assert_eq!(conf.thumb_size, THUMB_SIZE);
        assert_eq!(conf.database_path, DATA_DIR);
    }

    #[test]
    fn deserialize_conf_from_a_bunch_of_files() {
        create_test_conf();

        let conf = ServerConfigurationBuilder::new()
            .read_file("first_missing_configuration_file.json")
            .read_file("/tmp/conf.json")
            .read_file("second_missing_configuration_file.json")
            .build();

        assert_eq!(conf.document_root, DOCUMENT_ROOT);
        assert_eq!(conf.image_path, IMAGES_DIR);
        assert_eq!(conf.thumb_path, THUMBS_DIR);
        assert_eq!(conf.thumb_size, THUMB_SIZE);
        assert_eq!(conf.database_path, DATA_DIR);
    }

    #[test]
    fn accepts_manual_override_of_a_deserialized_conf() {
        create_test_conf();

        let conf = ServerConfigurationBuilder::new()
            .read_file("/tmp/conf.json")
            .with_document_root("/tmp/my/document_root")
            .with_image_path("/tmp/my/images")
            .with_thumb_path("/tmp/my/thumbs")
            .with_thumb_size(150)
            .with_database_path("/tmp/my/data")
            .build();

        assert_eq!(conf.document_root, "/tmp/my/document_root");
        assert_eq!(conf.image_path, "/tmp/my/images");
        assert_eq!(conf.thumb_path, "/tmp/my/thumbs");
        assert_eq!(conf.thumb_size, 150);
        assert_eq!(conf.database_path, "/tmp/my/data");
    }

    fn create_test_conf() {
        // create conf.json in /tmp
        let data = format!(
            r#"{{"document_root":"{}","image_path":"{}","thumb_path":"{}","thumb_size":{},"database_path":"{}"}}"#,
            DOCUMENT_ROOT, IMAGES_DIR, THUMBS_DIR, THUMB_SIZE, DATA_DIR
        );

        let mut buffer = std::fs::File::create("/tmp/conf.json").unwrap();

        buffer.write(&data.into_bytes()).unwrap();
    }
}
