use crate::model::*;

use chrono::{DateTime, Duration, Utc};
use std::fmt;

#[derive(Debug)]
pub enum CalendarBuilderError {
    PharmaciesNotDefined,
    ShiftCConfigurationNotDefined,
    StartDateNotDefined,
    EndDateNotDefined,
    InvalidPeriod,
}

impl fmt::Display for CalendarBuilderError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            CalendarBuilderError::EndDateNotDefined => write!(f, "End date not defined"),
            CalendarBuilderError::InvalidPeriod => write!(f, "Invalid period"),
            CalendarBuilderError::PharmaciesNotDefined => write!(f, "Pharmacies not defined"),
            CalendarBuilderError::ShiftCConfigurationNotDefined => {
                write!(f, "Shift configuration not defined")
            }
            CalendarBuilderError::StartDateNotDefined => write!(f, "Start date not defined"),
        }
    }
}

pub struct CalendarBuilder<'a> {
    pharmacies: Vec<&'a Pharmacy>,
    shift_configuration: Option<&'a ShiftConfiguration>,
    start_date: Option<DateTime<Utc>>,
    end_date: Option<DateTime<Utc>>,
}

impl<'a> CalendarBuilder<'a> {
    pub fn new() -> CalendarBuilder<'a> {
        CalendarBuilder {
            pharmacies: Vec::new(),
            shift_configuration: None,
            start_date: None,
            end_date: None,
        }
    }

    // Non dovrebbe essere un Vec<&Pharmacy> ???
    // In questo caso clonare il vettore non dovrebbe alterare il lifetime
    // e dovrei essere in grado di ordinarlo senza problemi
    pub fn using_pharmacies(mut self, pharmacies: &'a [Pharmacy]) -> CalendarBuilder<'a> {
        for pharmacy in pharmacies {
            self.pharmacies.push(&pharmacy);
        }
        self.pharmacies.sort_by_key(|a| a.sequence);
        self
    }

    pub fn using_shift_configuration(
        mut self,
        shift_configuration: &'a ShiftConfiguration,
    ) -> CalendarBuilder<'a> {
        self.shift_configuration = Some(shift_configuration);
        self
    }

    pub fn from(mut self, date: DateTime<Utc>) -> CalendarBuilder<'a> {
        self.start_date = Some(date);
        self
    }

    pub fn to(mut self, date: DateTime<Utc>) -> CalendarBuilder<'a> {
        self.end_date = Some(date);
        self
    }

    pub fn build(self) -> Result<Calendar<'a>, CalendarBuilderError> {
        if self.pharmacies.is_empty() {
            return Err(CalendarBuilderError::PharmaciesNotDefined);
        }

        if self.shift_configuration.is_none() {
            return Err(CalendarBuilderError::ShiftCConfigurationNotDefined);
        }

        if self.start_date.is_none() {
            return Err(CalendarBuilderError::StartDateNotDefined);
        }

        if self.end_date.is_none() {
            return Err(CalendarBuilderError::EndDateNotDefined);
        }

        if self.start_date.unwrap() > self.end_date.unwrap() {
            return Err(CalendarBuilderError::InvalidPeriod);
        }

        Ok(self.unroll())
    }

    fn shift_offset(config: &ShiftConfiguration, date: DateTime<Utc>) -> i64 {
        let delta = (date - config.start).num_days() as f64;
        let len = config.days as f64;
        (delta / len).floor() as i64
    }

    fn find_period(config: &'a ShiftConfiguration, offset: i64) -> Period {
        let start = config.start + Duration::days(offset * (config.days as i64));
        let len = Duration::days(config.days as i64);

        Period::new(start, len)
    }

    fn find_pharmacy_index(pharmacies: &[&Pharmacy], offset: i64) -> usize {
        offset as usize % pharmacies.len()
    }

    fn unroll(&self) -> Calendar<'a> {
        let shift_config = self.shift_configuration.unwrap();
        let start_date = self.start_date.unwrap();
        let end_date = self.end_date.unwrap();

        let offset = Self::shift_offset(shift_config, start_date);
        let mut period = Self::find_period(shift_config, offset);
        let mut pharmacy_index = Self::find_pharmacy_index(&self.pharmacies, offset);
        let mut cal = Calendar::new();

        println!("{} -> {}", period.start, end_date);

        while period.start < end_date {
            cal.append(period, self.pharmacies[pharmacy_index]);

            if pharmacy_index == self.pharmacies.len() - 1 {
                pharmacy_index = 0;
            } else {
                pharmacy_index += 1;
            }

            period.next();
        }

        cal
    }
}

impl<'a> Default for CalendarBuilder<'a> {
    fn default() -> Self {
        Self::new()
    }
}

#[allow(dead_code)]
#[allow(unused_imports)]
mod test {
    use crate::model::{Pharmacy, ShiftConfiguration};
    use crate::service::CalendarBuilder;
    use chrono::{DateTime, Duration, Utc};
    use std::str::FromStr;

    struct DataOwner {
        pharmacies: Vec<Pharmacy>,
        shift: ShiftConfiguration,
    }

    impl<'a> DataOwner {
        fn new() -> DataOwner {
            let sc = ShiftConfiguration {
                start: DateTime::<Utc>::from_str("2019-08-01T08:00:00Z").unwrap(),
                days: 7,
            };

            let mut data_owner = DataOwner {
                pharmacies: Vec::new(),
                shift: sc,
            };

            data_owner.pharmacies.push(Pharmacy {
                id: 1,
                name: "My drugstore".into(),
                address: "My drugstore's address".into(),
                sequence: 1,
                phone: "My drugstore's phone".into(),
                mine: true,
            });

            data_owner.pharmacies.push(Pharmacy {
                id: 2,
                name: "Drugstore #2".into(),
                address: "Drugstore #2 address".into(),
                sequence: 2,
                phone: "Drugstore #2 phone".into(),
                mine: false,
            });

            data_owner.pharmacies.push(Pharmacy {
                id: 3,
                name: "Drugstore #3".into(),
                address: "Drugstor #3 address".into(),
                sequence: 3,
                phone: "Drugstore #3 phone".into(),
                mine: false,
            });

            data_owner
        }

        pub fn get_pharmacies_ref(&self) -> Vec<&Pharmacy> {
            let mut pharmacies: Vec<&Pharmacy> = Vec::new();

            for p in &self.pharmacies {
                pharmacies.push(&p);
            }

            pharmacies
        }
    }

    #[test]
    fn offset_is_0_if_date_is_the_same_as_shift_start() {
        let data = DataOwner::new();

        assert_eq!(
            CalendarBuilder::shift_offset(&data.shift, data.shift.start),
            0
        );
    }

    #[test]
    fn offset_is_0_if_date_is_in_the_first_week() {
        let data = DataOwner::new();
        let offset = data.shift.start + Duration::days(1);

        assert_eq!(CalendarBuilder::shift_offset(&data.shift, offset), 0);
    }

    #[test]
    fn offset_is_0_if_date_is_last_day_of_the_first_week() {
        let data = DataOwner::new();
        let offset = data.shift.start + Duration::days(data.shift.days as i64 - 1);

        assert_eq!(0, CalendarBuilder::shift_offset(&data.shift, offset));
    }

    #[test]
    fn offset_is_1_if_date_is_the_second_week() {
        let data = DataOwner::new();
        let offset = data.shift.start + Duration::days(data.shift.days as i64);

        assert_eq!(1, CalendarBuilder::shift_offset(&data.shift, offset));
    }

    #[test]
    fn offset_is_minus_1_if_date_is_the_week_before() {
        let data = DataOwner::new();
        let offset = data.shift.start + Duration::days(-1);

        assert_eq!(-1, CalendarBuilder::shift_offset(&data.shift, offset));
    }

    #[test]
    fn offset_is_minus_2_if_date_is_two_weeks_before() {
        let data = DataOwner::new();
        let offset = data.shift.start + Duration::days((data.shift.days as i64 + 1) * -1);

        assert_eq!(-2, CalendarBuilder::shift_offset(&data.shift, offset));
    }

    #[test]
    fn period_with_offset_0_is_the_first_shift() {
        let data = DataOwner::new();

        let p = CalendarBuilder::find_period(&data.shift, 0);
        assert_eq!(p.start, data.shift.start);
        assert_eq!(
            p.end,
            data.shift.start + Duration::days(data.shift.days as i64)
        );
    }

    #[test]
    fn period_with_offset_1_is_the_second_shift() {
        let data = DataOwner::new();

        let p = CalendarBuilder::find_period(&data.shift, 1);
        assert_eq!(
            p.start,
            data.shift.start + Duration::days(data.shift.days as i64)
        );
        assert_eq!(
            p.end,
            data.shift.start + Duration::days((data.shift.days * 2) as i64)
        );
    }

    #[test]
    fn period_with_offset_minus1_is_the_shift_before_shift_start_date() {
        let data = DataOwner::new();

        let p = CalendarBuilder::find_period(&data.shift, -1);
        assert_eq!(
            p.start,
            data.shift.start - Duration::days(data.shift.days as i64)
        );
        assert_eq!(p.end, data.shift.start);
    }

    #[test]
    fn pharmacy_index_is_0_when_offset_is_0() {
        let data = DataOwner::new();
        assert_eq!(
            CalendarBuilder::find_pharmacy_index(&data.get_pharmacies_ref(), 0),
            0
        );
    }

    #[test]
    fn pharmacy_index_is_1_when_offset_is_1() {
        let data = DataOwner::new();
        assert_eq!(
            CalendarBuilder::find_pharmacy_index(&data.get_pharmacies_ref(), 1),
            1
        );
    }

    #[test]
    fn pharmacy_index_is_2_when_offset_is_2() {
        let data = DataOwner::new();
        assert_eq!(
            CalendarBuilder::find_pharmacy_index(&data.get_pharmacies_ref(), 2),
            2
        );
    }

    #[test]
    fn pharmacy_index_resets_when_offset_wraps() {
        let data = DataOwner::new();
        let offset = data.pharmacies.len() as i64;

        assert_eq!(
            CalendarBuilder::find_pharmacy_index(&data.get_pharmacies_ref(), offset),
            0
        );
    }

    #[test]
    fn unrolling_calendar_on_shift_start_date_returns_the_first_shift() {
        let data = DataOwner::new();

        let builder = CalendarBuilder {
            pharmacies: data.get_pharmacies_ref(),
            shift_configuration: Some(&data.shift),
            start_date: Some(data.shift.start),
            end_date: Some(data.shift.start + Duration::days(1)),
        };

        let cal = builder.unroll();

        assert_eq!(cal.shifts.len(), 1);
        assert_eq!(cal.shifts[0].period.start, data.shift.start);
        assert_eq!(
            cal.shifts[0].period.end,
            data.shift.start + Duration::days(data.shift.days as i64)
        );
        assert_eq!(cal.shifts[0].pharmacy, &data.pharmacies[0]);
    }

    #[test]
    fn unrolling_calendar_on_last_day_on_first_shift_returns_the_first_shift() {
        let data = DataOwner::new();

        let builder = CalendarBuilder {
            pharmacies: data.get_pharmacies_ref(),
            shift_configuration: Some(&data.shift),
            start_date: Some(data.shift.start),
            end_date: Some(data.shift.start + Duration::days(data.shift.days as i64)),
        };

        let cal = builder.unroll();

        assert_eq!(cal.shifts.len(), 1);
        assert_eq!(cal.shifts[0].period.start, data.shift.start);
        assert_eq!(
            cal.shifts[0].period.end,
            data.shift.start + Duration::days(data.shift.days as i64)
        );
        assert_eq!(cal.shifts[0].pharmacy, &data.pharmacies[0]);
    }

    #[test]
    fn unrolling_calendar_on_first_two_weeks_returns_the_first_two_shifts() {
        let data = DataOwner::new();

        let builder = CalendarBuilder {
            pharmacies: data.get_pharmacies_ref(),
            shift_configuration: Some(&data.shift),
            start_date: Some(data.shift.start),
            end_date: Some(data.shift.start + Duration::days(data.shift.days as i64 + 1)),
        };

        let cal = builder.unroll();

        assert_eq!(cal.shifts.len(), 2);

        assert_eq!(cal.shifts[0].period.start, data.shift.start);
        assert_eq!(
            cal.shifts[0].period.end,
            data.shift.start + Duration::days(data.shift.days as i64)
        );
        assert_eq!(cal.shifts[0].pharmacy, &data.pharmacies[0]);

        assert_eq!(
            cal.shifts[1].period.start,
            data.shift.start + Duration::days(data.shift.days as i64)
        );
        assert_eq!(
            cal.shifts[1].period.end,
            data.shift.start + Duration::days(data.shift.days as i64 * 2)
        );
        assert_eq!(cal.shifts[1].pharmacy, &data.pharmacies[1]);
    }

    #[test]
    fn unrolling_calendar_on_three_shifts() {
        let data = DataOwner::new();

        let builder = CalendarBuilder {
            pharmacies: data.get_pharmacies_ref(),
            shift_configuration: Some(&data.shift),
            start_date: Some(data.shift.start),
            end_date: Some(data.shift.start + Duration::days(data.shift.days as i64 * 2 + 1)),
        };

        let cal = builder.unroll();

        assert_eq!(cal.shifts.len(), 3);

        assert_eq!(cal.shifts[0].period.start, data.shift.start);
        assert_eq!(
            cal.shifts[0].period.end,
            data.shift.start + Duration::days(data.shift.days as i64)
        );
        assert_eq!(cal.shifts[0].pharmacy, &data.pharmacies[0]);

        assert_eq!(
            cal.shifts[1].period.start,
            data.shift.start + Duration::days(data.shift.days as i64)
        );
        assert_eq!(
            cal.shifts[1].period.end,
            data.shift.start + Duration::days(data.shift.days as i64 * 2)
        );
        assert_eq!(cal.shifts[1].pharmacy, &data.pharmacies[1]);

        assert_eq!(
            cal.shifts[2].period.start,
            data.shift.start + Duration::days(data.shift.days as i64 * 2)
        );
        assert_eq!(
            cal.shifts[2].period.end,
            data.shift.start + Duration::days(data.shift.days as i64 * 3)
        );
        assert_eq!(cal.shifts[2].pharmacy, &data.pharmacies[2]);
    }

    #[test]
    fn unrolling_calendar_on_four_shifts() {
        let data = DataOwner::new();

        let builder = CalendarBuilder {
            pharmacies: data.get_pharmacies_ref(),
            shift_configuration: Some(&data.shift),
            start_date: Some(data.shift.start),
            end_date: Some(data.shift.start + Duration::days(data.shift.days as i64 * 3 + 1)),
        };

        let cal = builder.unroll();

        assert_eq!(cal.shifts.len(), 4);

        assert_eq!(cal.shifts[0].period.start, data.shift.start);
        assert_eq!(
            cal.shifts[0].period.end,
            data.shift.start + Duration::days(data.shift.days as i64)
        );
        assert_eq!(cal.shifts[0].pharmacy, &data.pharmacies[0]);

        assert_eq!(
            cal.shifts[1].period.start,
            data.shift.start + Duration::days(data.shift.days as i64)
        );
        assert_eq!(
            cal.shifts[1].period.end,
            data.shift.start + Duration::days(data.shift.days as i64 * 2)
        );
        assert_eq!(cal.shifts[1].pharmacy, &data.pharmacies[1]);

        assert_eq!(
            cal.shifts[2].period.start,
            data.shift.start + Duration::days(data.shift.days as i64 * 2)
        );
        assert_eq!(
            cal.shifts[2].period.end,
            data.shift.start + Duration::days(data.shift.days as i64 * 3)
        );
        assert_eq!(cal.shifts[2].pharmacy, &data.pharmacies[2]);

        assert_eq!(
            cal.shifts[3].period.start,
            data.shift.start + Duration::days(data.shift.days as i64 * 3)
        );
        assert_eq!(
            cal.shifts[3].period.end,
            data.shift.start + Duration::days(data.shift.days as i64 * 4)
        );
        assert_eq!(cal.shifts[3].pharmacy, &data.pharmacies[0]);
    }

    #[test]
    fn unrolling_calendar_on_four_shifts_starting_on_second_week() {
        let data = DataOwner::new();

        let builder = CalendarBuilder {
            pharmacies: data.get_pharmacies_ref(),
            shift_configuration: Some(&data.shift),
            start_date: Some(data.shift.start + Duration::days(data.shift.days as i64)),
            end_date: Some(data.shift.start + Duration::days(data.shift.days as i64 * 5)),
        };

        let cal = builder.unroll();

        assert_eq!(cal.shifts.len(), 4);

        assert_eq!(
            cal.shifts[0].period.start,
            data.shift.start + Duration::days(data.shift.days as i64)
        );
        assert_eq!(
            cal.shifts[0].period.end,
            data.shift.start + Duration::days(data.shift.days as i64 * 2)
        );
        assert_eq!(cal.shifts[0].pharmacy, &data.pharmacies[1]);

        assert_eq!(
            cal.shifts[1].period.start,
            data.shift.start + Duration::days(data.shift.days as i64 * 2)
        );
        assert_eq!(
            cal.shifts[1].period.end,
            data.shift.start + Duration::days(data.shift.days as i64 * 3)
        );
        assert_eq!(cal.shifts[1].pharmacy, &data.pharmacies[2]);

        assert_eq!(
            cal.shifts[2].period.start,
            data.shift.start + Duration::days(data.shift.days as i64 * 3)
        );
        assert_eq!(
            cal.shifts[2].period.end,
            data.shift.start + Duration::days(data.shift.days as i64 * 4)
        );
        assert_eq!(cal.shifts[2].pharmacy, &data.pharmacies[0]);

        assert_eq!(
            cal.shifts[3].period.start,
            data.shift.start + Duration::days(data.shift.days as i64 * 4)
        );
        assert_eq!(
            cal.shifts[3].period.end,
            data.shift.start + Duration::days(data.shift.days as i64 * 5)
        );
        assert_eq!(cal.shifts[3].pharmacy, &data.pharmacies[1]);
    }
}
