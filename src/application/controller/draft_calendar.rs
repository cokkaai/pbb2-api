use rocket::{http::RawStr, State};
use chrono::{DateTime, Utc};

use crate::application::response_builder::ResponseBuilder;
use crate::service::{Database, JfsDatabase};
use crate::service::CalendarBuilder;

#[get("/draft/calendar?<start_date>&<end_date>")]
pub fn calc(
    database: State<JfsDatabase>,
    start_date: &RawStr,
    end_date: &RawStr,
) -> ResponseBuilder {
    debug!(
        "Computing draft calendar between {} and {}",
        start_date, end_date
    );

    let start = match parse_timestamp_string(start_date) {
        Ok(d) => d,
        Err(_) => return ResponseBuilder::bad_request()
    };

    let end = match parse_timestamp_string(end_date) {
        Ok(d) => d,
        Err(_) => return ResponseBuilder::bad_request()
    };

    let pharmacies = match database.get_pharmacies() {
        Err(e) => {
            error!("Cannot get pharmacy list: {}", e);
            return ResponseBuilder::bad_request();
        }
        Ok(pharmacies) => pharmacies,
    };

    let shift_config = match database.get_shift() {
        Ok(Some(x)) => x,
        Ok(None) => {
            error!("Missing draft shift configuration");
            return ResponseBuilder::bad_request();
        }
        Err(e) => {
            error!("Cannot get draft shift configuration: {}", e);
            return ResponseBuilder::bad_request();
        }
    };

    let cal = CalendarBuilder::new()
        .using_pharmacies(&pharmacies)
        .using_shift_configuration(&shift_config)
        .from(start)
        .to(end)
        .build();

    match cal {
        Ok(cal) => {
            debug!("Result: {:?}", cal);
            let body = serde_json::to_string(&cal).unwrap();
            ResponseBuilder::new().with_body(body)
        }
        Err(e) => {
            error!("Cannot compute calendar: {}", e);
            e.into()
        }
    }
}

fn parse_timestamp_string(timestamp: &RawStr) -> Result<DateTime<Utc>, ()>{
    match timestamp.url_decode() {
        Ok(s) => match DateTime::parse_from_rfc3339(&s) {
            Ok(d) => {
                trace!("Parsed date as {}", d);
                Ok(d.with_timezone(&Utc))
            }
            Err(e) => {
                error!("Cannot convert date {} in timestamp: {}", timestamp, e);
                Err(())
            }
        },
        Err(e) => {
            error!("Cannot convert date {} in timestamp: {}", timestamp, e);
            Err(())
        }
    }
}
