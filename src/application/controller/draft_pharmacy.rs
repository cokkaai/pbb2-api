use rocket::{http, State};
use rocket_contrib::json::Json;

use crate::model::{Pharmacy, NewPharmacy};
use crate::application::response_builder::ResponseBuilder;
use crate::service::{Database, JfsDatabase};

#[get("/draft/pharmacy")]
pub fn get_all(database: State<JfsDatabase>) -> ResponseBuilder {
    debug!("Reading all pharmacies");

    match database.get_pharmacies() {
        Err(error) => {
            error!("Error reading pharmacies: {}", &error);
            error.into()
        }
        Ok(pharmacies) => {
            debug!("Pharmacies read");
            let body = serde_json::to_string(&pharmacies).unwrap();
            ResponseBuilder::new().with_body(body)
        }
    }
}

#[post("/draft/pharmacy", data = "<pharmacy>")]
pub fn create(
    pharmacy: Json<NewPharmacy>,
    database: State<JfsDatabase>,
) -> ResponseBuilder {
    debug!("Creating pharmacy {:?}", pharmacy);

    match database.create_pharmacy(&pharmacy) {
        Err(error) => {
            error!("Error creating pharmacy");
            error.into()
        }
        Ok(id) => {
            debug!("Pharmacy created");
            ResponseBuilder::created(id.to_string())
        }
    }
}

// GET/PUT/DELETE /draft/pharmacy/{pharmacy_id} -> Pharmacy

#[get("/draft/pharmacy/<id>")]
pub fn get(id: usize, database: State<JfsDatabase>) -> ResponseBuilder {
    debug!("Reading pharmacy {}", id);

    match database.get_pharmacy(id) {
        Err(error) => {
            error!("Error reading pharmacy {}: {}", id, error);
            error.into()
        }
        Ok(None) => {
            warn!("Pharmacy not found with id {}", id);
            ResponseBuilder::new().with_status(http::Status::NotFound)
        }
        Ok(Some(pharmacy)) => {
            debug!("Pharmacy read");
            let body = serde_json::to_string(&pharmacy).unwrap();
            ResponseBuilder::new().with_body(body)
        }
    }
}

#[put("/draft/pharmacy/<id>", data = "<pharmacy>")]
pub fn update(
    id: usize,
    pharmacy: Json<Pharmacy>,
    database: State<JfsDatabase>,
) -> ResponseBuilder {
    debug!("Updating pharmacy {} with data: {:?}", id, pharmacy);

    let mut temp = pharmacy.clone();
    temp.id = id;

    match database.update_pharmacy(&temp) {
        Err(error) => {
            error!("Error updating pharmacy {:?}: {}", temp, error);
            error.into()
        }
        Ok(_) => {
            debug!("Pharmacy {} updated", temp.id);
            ResponseBuilder::ok()
        }
    }
}

#[delete("/draft/pharmacy/<id>")]
pub fn delete(id: usize, database: State<JfsDatabase>) -> ResponseBuilder {
    debug!("Deleting pharmacy {}", id);

    match database.delete_pharmacy(id) {
        Err(error) => {
            error!("Error deleting pharmacy {}: {}", id, error);
            error.into()
        }
        Ok(pharmacy) => {
            debug!("Pharmacy {} deleted", id);
            let body = serde_json::to_string(&pharmacy).unwrap();
            ResponseBuilder::new().with_body(body)
        }
    }
}
