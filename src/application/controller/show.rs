// vim: ts=4 expandtab

use rocket::State;

use crate::application::response_builder::ResponseBuilder;
use crate::model::{Configuration, Contents, Document, Pharmacy};
use crate::service::CalendarBuilder;
use crate::service::{Database, JfsDatabase};

const SHOW_TIME: u32 = 15;
const FADE_TIME: u32 = 2;

#[get("/show")]
pub fn get(database: State<JfsDatabase>) -> ResponseBuilder {
    debug!("Reading release configuration");

    match database.inner().get_release_configuration() {
        Err(error) => {
            error!("Error reading release configuration: {}", error);
            error.into()
        }
        Ok(data) => {
            debug!("Release configuration read");

            let msg = match message(&data) {
                Ok(msg) => msg,
                Err(e) => {
                    error!("{}", e);
                    return ResponseBuilder::no_content();
                }
            };

            let contents = Contents {
                my_pharmacy: my_pharmacy(&data),
                documents: documents(&data),
                message: msg,
                show_time: show_time(&data),
                fade_time: fade_time(&data),
                color_theme: color_theme(&data),
                clock: clock(&data),
                version: data.version,
            };

            let body = serde_json::to_string(&contents).unwrap();

            ResponseBuilder::new().with_body(body)
        }
    }
}

fn show(document: &Document) -> bool {
    let now = Some(chrono::Utc::now());

    if document.publish.is_none() || document.publish > now {
        return false;
    }

    if document.revoke.is_some() && document.revoke < now {
        return false;
    }

    true
}

fn my_pharmacy(configuration: &Configuration) -> Option<&Pharmacy> {
    let my_pharmacy = configuration
        .pharmacies
        .as_slice()
        .iter()
        .filter(|x| x.mine)
        .collect::<Vec<&Pharmacy>>();

    if my_pharmacy.is_empty() {
        None
    } else {
        Some(my_pharmacy[0])
    }
}

fn documents(configuration: &Configuration) -> Vec<&Document> {
    configuration
        .documents
        .as_slice()
        .iter()
        .filter(|x| show(&x))
        .collect()
}

fn message(configuration: &Configuration) -> Result<&str, String> {
    let shift = match configuration.shift {
        Some(ref shift) => shift,
        None => {
            return Err("Shift is not defined, unable to calculare a calendar".into());
        }
    };

    let cal = CalendarBuilder::new()
        .using_pharmacies(&configuration.pharmacies)
        .using_shift_configuration(shift)
        .from(chrono::Utc::now())
        .to(chrono::Utc::now())
        .build();

    match cal {
        Err(e) => Err(format!("{}", e)),
        Ok(cal) => {
            if cal.shifts[0].pharmacy.mine {
                Ok(on_duty_text(&configuration))
            } else {
                Ok(daily_opening_text(&configuration))
            }
        }
    }
}

fn show_time(configuration: &Configuration) -> u32 {
    match configuration.presentation {
        Some(ref presentation) => presentation.show_time,
        None => SHOW_TIME,
    }
}

fn fade_time(configuration: &Configuration) -> u32 {
    match configuration.presentation {
        Some(ref presentation) => presentation.fade_time,
        None => FADE_TIME,
    }
}

fn color_theme(configuration: &Configuration) -> &str {
    match configuration.presentation {
        Some(ref presentation) => &presentation.color_theme,
        None => "",
    }
}

fn clock(configuration: &Configuration) -> bool {
    match configuration.presentation {
        Some(ref presentation) => presentation.clock,
        None => false,
    }
}

fn on_duty_text(configuration: &Configuration) -> &str {
    match configuration.presentation {
        Some(ref presentation) => &presentation.on_duty_text,
        None => "",
    }
}

fn daily_opening_text(configuration: &Configuration) -> &str {
    match configuration.presentation {
        Some(ref presentation) => &presentation.daily_opening_text,
        None => "",
    }
}
