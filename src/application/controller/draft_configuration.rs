use rocket::State;
use rocket_contrib::json::Json;

use crate::model::Configuration;
use crate::application::response_builder::ResponseBuilder;
use crate::service::{Database, JfsDatabase};

#[get("/draft")]
pub fn get(database: State<JfsDatabase>) -> ResponseBuilder {
    debug!("Reading draft configuration");

    match database.inner().get_draft_configuration() {
        Err(error) => {
            error!("Error reading draft configuration: {}", error);
            error.into()
        }
        Ok(data) => {
            debug!("Draft configuration read");
            let body = serde_json::to_string(&data).unwrap();
            ResponseBuilder::new().with_body(body)
        }
    }
}

#[put("/draft", data = "<conf>")]
pub fn update(
    mut conf: Json<Configuration>,
    database: State<JfsDatabase>,
) -> ResponseBuilder {
    debug!("Updating draft configuration");

    conf.incr_version();

    match database.store_draft_configuration(conf.0) {
        Err(error) => {
            error!("Error updating draft configuration: {}", error);
            error.into()
        }
        Ok(_) => {
            debug!("Draft configuration updated");
            ResponseBuilder::ok()
        }
    }
}
