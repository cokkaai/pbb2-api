use rocket::State;
use rocket_contrib::json::Json;

use crate::model::ShiftConfiguration;
use crate::application::response_builder::ResponseBuilder;
use crate::service::{Database, JfsDatabase};

#[get("/draft/shift")]
pub fn get(database: State<JfsDatabase>) -> ResponseBuilder {
    debug!("Reading shift");

    match database.get_shift() {
        Err(error) => {
            error!("Error reading shift: {}", &error);
            error.into()
        }
        Ok(None) => {
            debug!("Still no shift defined");
            ResponseBuilder::ok()
        }
        Ok(Some(data)) => {
            debug!("Shift read");
            let body = serde_json::to_string(&data).unwrap();
            ResponseBuilder::new().with_body(body)
        }
    }
}

#[post("/draft/shift", data = "<shift>")]
pub fn create(
    shift: Json<ShiftConfiguration>,
    database: State<JfsDatabase>,
) -> ResponseBuilder {
    debug!("Creating shift {:?}", shift);

    match database.store_shift(&shift) {
        Err(error) => {
            error!("Error creating shift: {}", error);
            error.into()
        }
        Ok(_) => {
            debug!("Shift created");
            ResponseBuilder::ok()
        }
    }
}

#[put("/draft/shift", data = "<shift>")]
pub fn update(
    shift: Json<ShiftConfiguration>,
    database: State<JfsDatabase>,
) -> ResponseBuilder {
    debug!("Updating shift {:?}", shift);

    match database.store_shift(&shift) {
        Err(error) => {
            error!("Error updating shift: {}", &error);
            error.into()
        }
        Ok(_) => {
            debug!("Shift updated");
            ResponseBuilder::ok()
        }
    }
}
