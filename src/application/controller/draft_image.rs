use rocket::State;
use rocket_contrib::json::Json;
use std::io::Cursor;
use std::time::Instant;

use crate::model::Blob;
use crate::service::ServerConfiguration;
use crate::application::response_builder::ResponseBuilder;

fn log_duration(started: Instant, msg: &str) -> Instant {
    let now = Instant::now();

    trace!("{} ({} millis)", msg, now.duration_since(started).as_millis());

    now
}

#[post("/draft/image", data = "<blob>")]
pub fn create(blob: Json<Blob>, conf: State<ServerConfiguration>) -> ResponseBuilder {
    debug!("Uploading new image: {}", &blob.id);

    let mut t = Instant::now();

    // Decode base64 data
    let image = match base64::decode(&blob.data) {
        Ok(bytes) => bytes,
        Err(e) => {
            error!("{}", e);
            return ResponseBuilder::bad_request();
        }
    };

    t = log_duration(t, "Image decoded from base64");

    // Save data in image
    let path = std::path::Path::new(&conf.image_path).join(&blob.id);
    match std::fs::write(&path, &image) {
        Ok(_) => debug!("Image saved"),
        Err(e) => {
            error!("{}", e);
            return ResponseBuilder::internal_server_error();
        }
    }

    t = log_duration(t, "Image saved");

    // Save thumb
    let path = std::path::Path::new(&conf.thumb_path).join(&blob.id);
    let reader = match image::io::Reader::new(Cursor::new(&image)).with_guessed_format() {
        Ok(reader) => reader,
        Err(e) => {
            error!("{}", e);
            return ResponseBuilder::internal_server_error();
        }
    };

    t = log_duration(t, "[RESIZE] image::io::Reader created");

    let thumb = match reader.decode() {
        Ok(image) => image,
        Err(e) => {
            error!("{}", e);
            return ResponseBuilder::internal_server_error();
        }
    };
    t = log_duration(t, "[RESIZE] Reader decoded the image");

    match thumb
        .thumbnail(conf.thumb_size, conf.thumb_size)
        .save(&path)
    {
        Ok(_) => debug!("Thumbnail saved"),
        Err(e) => {
            error!("{}", e);
            return ResponseBuilder::internal_server_error();
        }
    }
    let _ = log_duration(t, "[RESIZE] Thumbnail created and saved");

    ResponseBuilder::created(blob.id.clone())
}

#[delete("/draft/image/<id>")]
pub fn delete(id: String, conf: State<ServerConfiguration>) -> ResponseBuilder {
    debug!("Deleting image {}", &id);

    let image = std::path::Path::new(&conf.image_path).join(&id);
    let thumb = std::path::Path::new(&conf.thumb_path).join(&id);

    std::fs::remove_file(image)
    .and_then(|_| {
        debug!("Image deleted");
        std::fs::remove_file(thumb)
    })
    .map(|_| {
        debug!("Thumbnail deleted");
        ResponseBuilder::ok()
    })
    .or_else::<ResponseBuilder, _>(|e| {
        error!("{}", e);
        Ok(ResponseBuilder::internal_server_error())
    })
    .unwrap()
}
