use rocket::{http, State};
use rocket_contrib::json::Json;

use crate::model::{Document, NewDocument};
use crate::application::response_builder::ResponseBuilder;
use crate::service::{Database, JfsDatabase};

#[get("/draft/document")]
pub fn get_all(database: State<JfsDatabase>) -> ResponseBuilder {
    debug!("Reading all documents");

    match database.get_documents() {
        Err(error) => {
            error!("Error reading documents: {}", error);
            error.into()
        }
        Ok(documents) => {
            let body = serde_json::to_string(&documents).unwrap();
            ResponseBuilder::new().with_body(body)
        }
    }
}

#[post("/draft/document", data = "<document>")]
pub fn create(
    document: Json<NewDocument>,
    database: State<JfsDatabase>,
) -> ResponseBuilder {
    debug!("Creating new document: {:?}", &document);

    match database.create_document(&document) {
        Err(error) => {
            error!("Error creating document {:?}: {}", &document, error);
            error.into()
        }
        Ok(id) => {
            debug!("Document created");
            ResponseBuilder::created(id.to_string())
        }
    }
}

// GET/PUT/DELETE /draft/documents/{document_id} -> Document

#[get("/draft/document/<id>")]
pub fn get(id: usize, database: State<JfsDatabase>) -> ResponseBuilder {
    debug!("Reading document {}", &id);

    match database.get_document(id) {
        Err(error) => {
            error!("Error reading document {}: {}", &id, &error);
            error.into()
        }
        Ok(None) => {
            debug!("Document read");
            ResponseBuilder::new().with_status(http::Status::NotFound)
        }
        Ok(Some(document)) => {
            debug!("Document read");
            let body = serde_json::to_string(&document).unwrap();
            ResponseBuilder::new().with_body(body)
        }
    }
}

#[put("/draft/document/<id>", data = "<document>")]
pub fn update(
    id: usize,
    document: Json<Document>,
    database: State<JfsDatabase>,
) -> ResponseBuilder {
    debug!("Updating document {} with data: {:?}", &id, &document);

    let mut temp = document.clone();
    temp.id = id;

    match database.update_document(&temp) {
        Err(error) => {
            error!("Error updating document {:?}: {}", temp, &error);
            error.into()
        }
        Ok(_) => {
            debug!("Document updated");
            ResponseBuilder::ok()
        }
    }
}

#[delete("/draft/document/<id>")]
pub fn delete(id: usize, database: State<JfsDatabase>) -> ResponseBuilder {
    debug!("Deleting document {}", &id);

    match database.delete_document(id) {
        Err(error) => {
            error!("Error deleting document {}: {}", id, error);
            error.into()
        }
        Ok(document) => {
            debug!("Document {} deleted", id);
            let body = serde_json::to_string(&document).unwrap();
            ResponseBuilder::new().with_body(body)
        }
    }
}
