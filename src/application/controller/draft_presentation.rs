use rocket::State;
use rocket_contrib::json::Json;

use crate::model::Presentation;
use crate::application::response_builder::ResponseBuilder;
use crate::service::{Database, JfsDatabase};

#[get("/draft/presentation")]
pub fn get(database: State<JfsDatabase>) -> ResponseBuilder {
    debug!("Reading presentation");

    match database.get_presentation() {
        Err(error) => {
            error!("Error reading presentation: {}", &error);
            error.into()
        }
        Ok(None) => {
            debug!("Still no presentation defined");
            ResponseBuilder::ok()
        }
        Ok(Some(data)) => {
            debug!("Presentation read");
            let body = serde_json::to_string(&data).unwrap();
            ResponseBuilder::new().with_body(body)
        }
    }
}

#[post("/draft/presentation", data = "<presentation>")]
pub fn create(
    presentation: Json<Presentation>,
    database: State<JfsDatabase>,
) -> ResponseBuilder {
    debug!("Creating presentation");

    match database.store_presentation(&presentation) {
        Err(error) => {
            error!("Error creating presentation: {}", error);
            error.into()
        }
        Ok(_) => {
            debug!("Presentation created");
            ResponseBuilder::ok()
        }
    }
}

#[put("/draft/presentation", data = "<presentation>")]
pub fn update(
    presentation: Json<Presentation>,
    database: State<JfsDatabase>,
) -> ResponseBuilder {
    debug!("Updating presentation");

    match database.store_presentation(&presentation) {
        Err(error) => {
            error!("Error updating presentation: {}", error);
            error.into()
        }
        Ok(_) => {
            debug!("Presentation updated");
            ResponseBuilder::ok()
        }
    }
}
