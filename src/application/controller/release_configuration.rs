use rocket::State;

use crate::application::response_builder::ResponseBuilder;
use crate::service::{Database, JfsDatabase};

#[get("/release")]
pub fn get(database: State<JfsDatabase>) -> ResponseBuilder {
    debug!("Reading release configuration");

    match database.inner().get_release_configuration() {
        Err(error) => {
            error!("Cannot read release configuration: {}", error);
            error.into()
        }
        Ok(data) => {
            debug!("Release configuration read");
            let body = serde_json::to_string(&data).unwrap();
            ResponseBuilder::new().with_body(body)
        }
    }
}

#[put("/release")]
pub fn update(database: State<JfsDatabase>) -> ResponseBuilder {
    debug!("Reading draft configuration");

    match database.inner().get_draft_configuration() {
        Err(error) => {
            error!("Error reading draft configuration: {}", error);
            error.into()
        }
        Ok(conf) => {
            debug!("Draft configuration read");

            match database.store_release_configuration(conf) {
                Err(error) => {
                    error!("Cannot update release configuration: {}", error);
                    error.into()
                }
                Ok(_) => {
                    debug!("Release configuration updated");
                    ResponseBuilder::ok()
                }
            }
        }
    }
}
