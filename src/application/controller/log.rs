use crate::model::Log;
use crate::application::response_builder::ResponseBuilder;
use rocket_contrib::json::Json;

#[post("/log/info", data = "<log>")]
pub fn log_info(log: Json<Log>) -> ResponseBuilder {
    info!("{:?}", &log);
    ResponseBuilder::ok()
}

#[post("/log/error", data = "<log>")]
pub fn log_error(log: Json<Log>) -> ResponseBuilder {
    error!("{:?}", &log);
    ResponseBuilder::ok()
}

#[post("/log/warning", data = "<log>")]
pub fn log_warning(log: Json<Log>) -> ResponseBuilder {
    warn!("{:?}", &log);
    ResponseBuilder::ok()
}
