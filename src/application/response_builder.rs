use crate::application::Application;
use crate::service::CalendarBuilderError;
use crate::service::DatabaseError;
use rocket::{http, request, response};
use std::io::Cursor;

#[derive(Debug)]
pub struct ResponseBuilder {
    status: http::Status,
    body: Option<String>,
    location: Option<String>,
}

impl ResponseBuilder {
    pub fn new() -> ResponseBuilder {
        ResponseBuilder {
            status: http::Status::Ok,
            body: None,
            location: None,
        }
    }

    pub fn with_status(mut self, status: http::Status) -> ResponseBuilder {
        self.status = status;
        self
    }

    pub fn with_body(mut self, body: String) -> ResponseBuilder {
        self.body = Some(body);
        self
    }

    pub fn with_location(mut self, location: String) -> ResponseBuilder {
        self.location = Some(location);
        self
    }

    pub fn ok() -> ResponseBuilder {
        Self::new()
    }

    pub fn created(location: String) -> ResponseBuilder {
        Self::new()
            .with_location(location)
    }

    pub fn no_content() -> ResponseBuilder {
        Self::new()
            .with_status(http::Status::NoContent)
    }

    pub fn bad_request() -> ResponseBuilder {
        Self::new()
            .with_status(http::Status::BadRequest)
    }

    pub fn internal_server_error() -> ResponseBuilder {
        Self::new()
            .with_status(http::Status::InternalServerError)
    }
}

impl<'r> response::Responder<'r> for ResponseBuilder {
    fn respond_to(self, _: &request::Request) -> response::Result<'r> {
        let mut response = response::Response::build();

        response
            .header(http::ContentType::JSON)
            .raw_header("X-Api-Version", Application::VERSION)
            .status(self.status);

        if let Some(body) = self.body {
            response.sized_body(Cursor::new(body));
        }

        if let Some(location) = self.location {
            response.header(http::hyper::header::Location(location));
        }

        response.ok()
    }
}

impl From<DatabaseError> for ResponseBuilder {
    fn from(error: DatabaseError) -> Self {
        match error {
            DatabaseError::InvalidId => ResponseBuilder::new().with_status(http::Status::NotFound),
            DatabaseError::DuplicatedEntry => ResponseBuilder::bad_request(),
            DatabaseError::IoError { base_error } => {
                error!("{}", base_error);
                ResponseBuilder::internal_server_error()
            }
            DatabaseError::Error { message } => {
                error!("{}", message);
                ResponseBuilder::internal_server_error()
            }
            _ => {
                error!("{}", error);
                ResponseBuilder::internal_server_error()
            }
        }
    }
}

impl From<CalendarBuilderError> for ResponseBuilder {
    fn from(error: CalendarBuilderError) -> Self {
        error!("{}", error);
        ResponseBuilder::bad_request()
    }
}

impl Default for ResponseBuilder {
    fn default() -> Self {
        Self::new()
    }
}
