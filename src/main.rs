use pbb3_api::application::Application;
use pbb3_api::service::ServerConfigurationBuilder;
use log::info;
use dotenv::dotenv;
use rocket_cors::Error;

fn main() -> Result<(), Error> {
    dotenv().ok();
    
    env_logger::init();

    let conf = ServerConfigurationBuilder::from_env()
        .build();

    info!("Server configuration: {:?}", &conf);

    let app = Application::new(conf);

    info!("API is running");

    app.run()
}
