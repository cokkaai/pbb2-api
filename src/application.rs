mod controller;
mod response_builder;

use crate::service::JfsDatabase;
use crate::service::ServerConfiguration;

use rocket::response::Redirect;
use rocket::http::Method;
use rocket_contrib::serve::StaticFiles;
use rocket_cors::{AllowedHeaders, AllowedOrigins, Error};

use std::convert::From;

pub struct Application {
    conf: ServerConfiguration,
}

impl Application {
    pub const VERSION: &'static str = "1.0.0";

    pub fn new(conf: ServerConfiguration) -> Application {
        Application { conf }
    }

    fn image_path(&self) -> &str {
        &self.conf.image_path
    }

    fn thumb_path(&self) -> &str {
        &self.conf.thumb_path
    }

    fn templates(&self) -> String {
        self.conf.document_root.clone() + "/templates"
    }

    fn images(&self) -> String {
        self.conf.document_root.clone() + "/img"
    }

    fn css(&self) -> String {
        self.conf.document_root.clone() + "/css"
    }

    fn js(&self) -> String {
        self.conf.document_root.clone() + "/js"
    }

    fn app(&self) -> String {
        self.conf.document_root.clone() + "/app"
    }

    fn preview(&self) -> String {
        self.conf.document_root.clone() + "/preview"
    }

    fn show(&self) -> String {
        self.conf.document_root.clone() + "/show"
    }

    pub fn run(&self) -> Result<(), Error> {
        let database = JfsDatabase::new_with_path(&self.conf.database_path);

        let allowed_origins = AllowedOrigins::all();

        let allowed_methods = vec![Method::Get, Method::Post, Method::Put, Method::Delete];

        // You can also deserialize this
        let cors = rocket_cors::CorsOptions {
            allowed_origins,
            allowed_methods: allowed_methods.into_iter().map(From::from).collect(),
            allowed_headers: AllowedHeaders::some(&["Authorization", "Accept"]),
            allow_credentials: true,
            ..Default::default()
        }
        .to_cors()?;

        rocket::ignite()
            .manage(database)
            .manage(self.conf.clone())
            // Default route is for web app
            .mount("/", routes![redirect_to_show])
            .mount("/app", StaticFiles::from(self.app()))
            .mount("/preview", StaticFiles::from(self.preview()))
            .mount("/show", StaticFiles::from(self.show()))
            .mount("/js", StaticFiles::from(&(self.js())))
            .mount("/css", StaticFiles::from(&(self.css())))
            .mount("/img", StaticFiles::from(&(self.images())))
            .mount("/templates", StaticFiles::from(&(self.templates())))
            // Static routes for user-uploaded contents
            .mount("/images", StaticFiles::from(self.image_path()))
            .mount("/thumbs", StaticFiles::from(self.thumb_path()))
            // API routes
            .mount(
                "/api",
                routes![
                    api_index,
                    controller::draft_configuration::get,
                    controller::draft_configuration::update,
                    controller::draft_shift::get,
                    controller::draft_shift::create,
                    controller::draft_shift::update,
                    controller::draft_presentation::get,
                    controller::draft_presentation::create,
                    controller::draft_presentation::update,
                    controller::draft_pharmacy::get_all,
                    controller::draft_pharmacy::get,
                    controller::draft_pharmacy::create,
                    controller::draft_pharmacy::update,
                    controller::draft_pharmacy::delete,
                    controller::draft_document::get_all,
                    controller::draft_document::get,
                    controller::draft_document::create,
                    controller::draft_document::update,
                    controller::draft_document::delete,
                    controller::draft_image::create,
                    controller::draft_image::delete,
                    controller::release_configuration::get,
                    controller::release_configuration::update,
                    controller::draft_calendar::calc,
                    controller::preview::get,
                    controller::show::get,
                    controller::log::log_info,
                    controller::log::log_warning,
                    controller::log::log_error
                ],
            )
            // Defines catchers for errors that may be triggered
            // by incorrect web app behaviour
            .register(catchers![bad_request, not_found])
            // Implement CORS
            .attach(cors)
            .launch();

        Ok(())
    }
}

#[catch(400)]
fn bad_request(req: &rocket::request::Request) -> String {
    let msg = format!("Got a bad request to '{}'.", req.uri());
    error!("{}", &msg);
    msg
}

#[catch(404)]
fn not_found(req: &rocket::request::Request) -> String {
    let msg = format!("The requested resource '{}' could not be found.", req.uri());
    error!("{}", &msg);
    msg
}

#[get("/")]
fn api_index() -> String {
    format!("pbb version {}", Application::VERSION)
}

#[get("/")]
fn redirect_to_show() -> Redirect {
    Redirect::to("/show/index.html")
}
