Cross compile to Raspberry Pi

Install gcc and ld for arm v6
# apt-get install gcc-arm-linux-gnueabihf binutils-arm-linux-gnueabihf

Buster package libgcc-s1-armhf-cross installs libgcc_s as libgcc_s.so.1
Cargo looks for libcc_s.so, so it is necessary to link it with the unversioned name.
# ln -s /usr/arm-linux-gnueabihf/lib/libgcc_s.so.1 /usr/arm-linux-gnueabihf/lib/libgcc_s.so

Install arm v6 rust target
$ rustup target install arm-unknown-linux-gnueabihf

Setup the arm linker for amr v6 in .cargo/config
[target.arm-unknown-linux-gnueabihf]
# Arm linker is /usr/bin/arm-linux-gnueabihf-ld
linker = "arm-linux-gnueabihf-ld"

Build the api
$ cargo build --release --target arm-unknown-linux-gnueabihf
